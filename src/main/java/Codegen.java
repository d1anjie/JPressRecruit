import io.jpress.codegen.AddonGenerator;

public class Codegen {

    private static String dbUrl = "jdbc:mysql://127.0.0.1:3306/jpress";
    private static String dbUser = "root";
    private static String dbPassword = "anjie7410";

    private static String addonName = "recruit";
    private static String dbTables = "jpress_addon_recruit,jpress_addon_recruit_resume";
    private static String modelPackage = "io.jpress.addon.recruit.model";
    private static String servicePackage = "io.jpress.addon.recruit.service";


    public static void main(String[] args) {

        AddonGenerator moduleGenerator = new AddonGenerator(addonName, dbUrl, dbUser, dbPassword, dbTables, modelPackage, servicePackage);
        moduleGenerator.setGenUI(true);
        moduleGenerator.gen();

    }

}
