package io.jpress.addon.recruit.controller;

import com.jfinal.aop.Inject;
import com.jfinal.kit.Ret;
import io.jboot.web.controller.JbootController;
import io.jboot.web.controller.annotation.RequestMapping;
import io.jpress.addon.recruit.model.JpressAddonRecruitResume;
import io.jpress.addon.recruit.service.JpressAddonRecruitResumeService;
import io.jpress.commons.utils.CommonsUtils;


@RequestMapping(value = "/recruitController", viewPath = "/")
public class JpressAddonRecruitResumeController extends JbootController {

    @Inject
    private JpressAddonRecruitResumeService service;

    public void doSave() {

        JpressAddonRecruitResume entry = getModel(JpressAddonRecruitResume.class,"jpressAddonRecruitResume");
        CommonsUtils.escapeModel(entry);
        if(entry.getName()==null||entry.getName().trim().equals("")){
            renderJson(Ret.fail().set("msg", "姓名不能是空"));
            return;
        }
        if(entry.getPhone()==null||entry.getPhone().trim().equals("")){
            renderJson(Ret.fail().set("msg", "电话不能是空"));
            return;
        }
        if(entry.getEmail()==null||entry.getEmail().trim().equals("")){
            renderJson(Ret.fail().set("msg", "邮箱不能是空"));
            return;
        }
        if(entry.getResume()==null||entry.getResume().trim().equals("")){
            renderJson(Ret.fail().set("msg", "简历不能是空"));
            return;
        }
        service.saveOrUpdate(entry);
        renderJson(Ret.ok().set("id", entry.getId()));

    }

}
