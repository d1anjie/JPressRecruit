package io.jpress.addon.recruit.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jpress.addon.recruit.service.JpressAddonRecruitResumeService;
import io.jpress.addon.recruit.model.JpressAddonRecruitResume;
import io.jboot.service.JbootServiceBase;

@Bean
public class JpressAddonRecruitResumeServiceProvider extends JbootServiceBase<JpressAddonRecruitResume> implements JpressAddonRecruitResumeService {

}