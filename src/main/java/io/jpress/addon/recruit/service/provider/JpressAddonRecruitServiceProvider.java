package io.jpress.addon.recruit.service.provider;

import io.jboot.aop.annotation.Bean;
import io.jpress.addon.recruit.service.JpressAddonRecruitService;
import io.jpress.addon.recruit.model.JpressAddonRecruit;
import io.jboot.service.JbootServiceBase;

@Bean
public class JpressAddonRecruitServiceProvider extends JbootServiceBase<JpressAddonRecruit> implements JpressAddonRecruitService {

    public JpressAddonRecruit findOne() {
        JpressAddonRecruit jpressAddonRecruit = getDao().findFirst("SELECT * FROM jpress_addon_recruit LIMIT 1");
        return jpressAddonRecruit;
    }

}
