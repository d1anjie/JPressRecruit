package io.jpress.addon.recruit.directive;

import com.jfinal.aop.Inject;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jpress.addon.recruit.model.JpressAddonRecruit;
import io.jpress.addon.recruit.service.JpressAddonRecruitService;


@JFinalDirective("recruitDirective")
public class RecruitDirective extends JbootDirectiveBase {

    @Inject
    private JpressAddonRecruitService service;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {
        JpressAddonRecruit jpressAddonRecruit = service.findOne();
        scope.setLocal("jpressAddonRecruit", jpressAddonRecruit);
        renderBody(env, scope, writer);
    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
